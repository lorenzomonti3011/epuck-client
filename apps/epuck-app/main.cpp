#include "RobotDriver.hpp"

#include <csignal>
#include <iostream>
#include <memory>



void incorrectArguments(const int& argc);

bool signaled = false;
void exitLoop(int sig) {
    std::cout << " Loop termination\n";
    signaled = true;
}

int main(int argc, char** argv) {

    Robot robot;
    if (argv[2] == NULL) incorrectArguments(argc);

    robot.ip = argv[2];
    std::unique_ptr<RobotDriver> driver;

    if(robot.ip.size() == 11 ) {
        driver = std::make_unique<EPuckV1Driver>(robot);
    } else if (robot.ip.size() == 13){
        driver = std::make_unique<EPuckV2Driver>(robot);
    } else if (robot.ip == "127.0.0.1"){  
        driver = std::make_unique<EPuckVREPDriver>(robot);
    } else {
        incorrectArguments(argc);
        return -1;
    }

    driver->init();
    std::string cont = argv[1];

    if (cont == "setWheelCmd") {
        if (argc != 5) {
            incorrectArguments(argc);
        } else {
            robot.c = setWheelCmd;
            robot.wheels_command.left_velocity = std::stol(argv[3]);
            robot.wheels_command.right_velocity  = std::stol(argv[4]);
        }
    } else if (cont == "setVel") {
        if (argc != 5) {
            incorrectArguments(argc);
        } else {
            robot.c = setVel;
            robot.lin_velocity = std::stod(argv[3]);
            robot.ang_velocity = std::stod(argv[4]);
        }
    } else if (cont == "visServo") {
        if (argc != 3)
            incorrectArguments(argc);
        else
            robot.c = visServo;
    } else if (cont == "followWall") {
        if (argc != 3)
            incorrectArguments(argc);
        else
            robot.c = followWall;
    } else {
        incorrectArguments(argc);
    }
    std::cout << "Controller is "<< argv[1] << "\n";
    std::signal(SIGINT, exitLoop);
    while (true) {
        driver->read();
        driver->sendCmd();
        if (signaled == true) {
            break;
        }
    }
}

void incorrectArguments(const int& argc) {
    std::cout << "There are "<< argc -1 << " arguments, which is incorrect.\n";
    std::cout <<
        "The first argument should be one of the "
        "following:"
        "\n\tsetWheelCmd\n\tsetRobVel\n\tfollowWall\n\tvisServo\n";
    std::cout << "The following arguments should be:\n";
    std::cout << "\tsetWheelCmd: IP leftWheelCmd rightWheelCmd\n";
    std::cout << "\tsetVel: IP v(linear vel) w(angular vel)\n";
    std::cout << "\tvisServo: IP\n";
    std::cout << "\tfollowWall: IP\n";
    exit(0);
}

